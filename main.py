import sys
from datetime import datetime, timezone

from requests import HTTPError

from utils import get_existing_results, save_results


def main(analytics_token):
    try:
        # Get existing results
        existing_results = get_existing_results(analytics_token)

        h5p_result = existing_results["count_h5p_statements"]
        moodle_result = existing_results["count_moodle_statements"]

        # Built results object (usually array)
        result = {
            "overall": [
                {
                    "h5p": h5p_result[0]["result"][0]["column2"],
                    "moodle": moodle_result[0]["result"][0]["column2"],
                },
            ],
            "h5p": [{"h5p": h5p_result[0]["result"][0]["column2"], "moodle": 0}],
            "moodle": [
                {
                    "h5p": 0,
                    "moodle": moodle_result[0]["result"][0]["column2"],
                }
            ],
        }

        # Send result to rights engine
        save_results(analytics_token, {"result": result})
    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1])
